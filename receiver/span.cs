using System;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace receiver {

    //public class debugstream : System.IO.Stream {
    //}

    [Serializable]
    [XmlRoot("span")]
    public class Span {
        private static readonly XmlSerializer decoder = new XmlSerializer(typeof(Span));

        public static async Task Decode(System.IO.Stream s, Action<Span, Exception> callback) {
            var settings = new XmlReaderSettings();
            // see https://stackoverflow.com/questions/18186225/c-sharp-xdocument-load-with-multiple-roots
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.IgnoreWhitespace = true;

            var cont = true;
            var buf = new byte[1];
            while (cont) {
                using (var ms = new System.IO.MemoryStream(1024)) {
                    // copy until a 0 byte or EOF
                    while (true) {
                        var count = await s.ReadAsync(buf, 0, 1);
                        if (count == 1) {
                            var b = buf[0];
                            if (b != 0) {
                                ms.WriteByte(b);
                                continue;
                            }
                            break; // b/c of 0 byte
                        }
                        cont = false; // b/c of count = 0, i.e. EOF. 
                        // we probably then decode an empty ms or an incomplete span leading to an exception
                    }
                    ms.Position = 0;
                    if (ms.Length > 0) {
                        using (var xr = XmlReader.Create(ms, settings)) {
                            // Deserialize disposes of xr
                            // see https://referencesource.microsoft.com/#system.xml/System/Xml/Serialization/XmlSerializer.cs,416
                            try {
                                var span = Span.decoder.Deserialize(xr);
                                callback(span as Span, null);
                            } catch (Exception ex) {
                                callback(null, ex);
                            }
                        }
                    }
                }
            } // while cont. Exit this loop, if we could ReadAsync only 0 bytes
        }

        [XmlAttribute()] public string s { get; set; }
        [XmlAttribute()] public int spid { get; set; }
        [XmlElement("event")] public string eventText { get; set; }

        public override string ToString() {
            return $"span ID:{s} SPID:{spid} says: {eventText}";
        }
    }
}