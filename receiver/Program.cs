﻿using System;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Threading.Tasks;
using System.Threading;

namespace receiver {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("This is the tcp Span XML receiver app");

            var t = rcv();
            t.Wait();
        }

        private async static Task rcv() {
            var token = new CancellationTokenSource().Token; // hack! need to keep the source

            var listener = new TcpListener(IPAddress.Loopback, 16578);
            Console.WriteLine($"Starting Listener {listener.LocalEndpoint}");
            listener.Start();
            try {
                var n = 0;
                while (true) {
                    Console.WriteLine($"waiting for connection {n}");
                    var cli = await listener.AcceptTcpClientAsync();
                    var _ = Accept(cli, token, n); // we intentionally don't await it
                    n++;
                }
            } finally {
                listener.Stop();
            }
        }

        private static async Task Accept(TcpClient cli, CancellationToken token, int n) {
            try {
                using (cli) {
                    Console.WriteLine($"connected {n}");
                    using (var s = cli.GetStream()) {
                        Console.WriteLine($"got stream {n}");
                        await Span.Decode(s, (span, ex) => {
                            if (ex != null) {
                                Console.WriteLine($"{n} ERROR: {ex.Message}"); // decode err
                                return;
                            }
                            Console.WriteLine($"{n}: {span}");
                        });
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            } finally {
                Console.WriteLine($"done {n}");
            }
        }
    }
}
