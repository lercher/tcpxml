﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace serialize {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("This is serializer");

            var settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.Async = true;

            var s1 = "<span s='S1' spid='77'><event>Hello there. General Kenobi.</event></span>";
            var s2 = "<span s='S2' spid='77'><event>Hello there. General Kenobi.</event></span>";
            using (var sr = new System.IO.StringReader(s1 + "\n" + s2 + "\n\n<span/>"))
            using (var xr = XmlReader.Create(sr, settings)) {
                while (true) {
                    try {
                        var span = Span.decoder.Deserialize(xr);
                        Console.WriteLine(span.ToString());
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                }
            }
        }
    }
}
