using System;
using System.Xml;
using System.Xml.Serialization;

namespace serialize {
    [Serializable]
    [XmlRoot("span")]
    public class Span {
        public static readonly XmlSerializer decoder = new XmlSerializer(typeof(Span));

        [XmlAttribute()] public string s { get; set; }
        [XmlAttribute()] public int spid { get; set; }
        [XmlElement("event")] public string eventText { get; set; }

        public override string ToString() {
            return $"span ID:{s} SPID:{spid} says: {eventText}";
        }
    }
}