﻿using System;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Threading.Tasks;
using System.Threading;

namespace sender {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("This is the XML tcp sender app");
            var t = send(200);
            t.Wait();
        }

        private static async Task send(int n) {
            var token = new CancellationTokenSource().Token; // hack! need to keep the source

            var settings = new XmlWriterSettings();
            settings.Async = true;
            settings.CloseOutput = false;
            settings.OmitXmlDeclaration = true;

            var cli = new TcpClient();
            await cli.ConnectAsync("localhost", 16578);
            using (var s = cli.GetStream()) {
                for (var i = 0; i < n; i++) {
                    var x = new XDocument(new XElement("span",
                        new XAttribute("s", "S" + i.ToString()),
                        new XAttribute("spid", "12345"),
                        new XElement("event",
                            "I'm new"
                        )
                    ));
                    //Console.WriteLine(x.ToString());
                    using (var xw = XmlWriter.Create(s, settings)) {
                        await x.WriteToAsync(xw, token);
                        await xw.FlushAsync();
                        s.WriteByte(0); // sending a 0 byte to signal the receiver that a <span> is complete
                    }
                }
            }
        }
    }
}
